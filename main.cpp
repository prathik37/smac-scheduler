/*
SMAC - Prathik Prakash

Code to schedule the given inputs. The current assumption is that the nurse/designated authority keys in the events
to be scheduled. Suitable objects are instantiated (done by Nachiket) which goes onto a priority queue (done by Sandesh)
sorted by deadline. The following program reads the event details from the queues, and tries to schedule as many events
as possible. To achieve this we are using the combination of start time and duration.
Our scheduling is similar to Rate Monotonic (RM) scheduling.

This program reads the following values from the task objects that are in priority queue. Queue is continuously monitored
to include new additions.

|  Deadline  |  Start time  |  Duration  | Task ID  | isScheduled |

All the values are assumed to be numbers with time starting from 0 and every integer corresponds to 1 Hour.
Our total time frame for a day is 24 units.

The values that are read is put in a vector of vectors which is sorted according to their combination of arrival time and their
deadline that is the tasks which arrive early with short durations are preferred as of now and the final schedule of
task ID's is printed which is taken as an input by a data structure (done by Vinay) which can be used to observe the
final schedule.
*/

#include <type_traits>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include "manip.h"
#define TSLOTS 10

using namespace std;

int main()
{
    int timeslots = TSLOTS;
    size_t i = 0;

    //Values from the objects in the priority queue are read
    vector< vector<int> > S = {
                   {3,1,1,0,3},
                   {4,0,1,0,5},
                   {4,1,3,0,7},
                   {5,2,2,0,9},
                   {7,2,4,0,10},
                   {8,2,5,0,4},
                   {10,6,2,0,2}
                   } ;

    cout<<"DESCRIPTION OF EACH ROW"<<endl;
    cout<<"DEADLINE START_TIME DURATION SCHEDULABLE TASK_ID"<<endl;
    cout<<"\n";

    cout<<"ENTERED SCHEDULE"<<endl;
    //Print schedule before scheduling
    print_schedule(S);
    cout<<"\n";

    //Sort schedule based on start time and duration
    sort_schedule(S);

    cout <<"SCHEDULE AFTER SORTING" << endl;
    //Print schedule after schedule
    print_schedule(S);
    cout<<"\n";

    try
    {
    //Till timeslots are available
    while(timeslots!=0)
    {
        //Pass reference to each vector within a vector so that it is manipulated
        vector<int> &acs = S.at(i++);

        //(Duration - Start time) < Deadline for event to be schedulable
        if(((acs[2]+acs[1])<=acs[0]) && (acs[2]<=timeslots))
        {
          timeslots = timeslots - acs[2];
          //Set schedulable to 1
          acs[3] = 1;
          cout<<acs[4]<<" CAN BE SCHEDULED"<<endl;
        }
        else
        cout<<acs[4]<<" CANNOT BE SCHEDULED"<<endl;

    }
    cout<<"\n";

    cout <<"FINAL SCHEDULE" << endl;
    print_schedule(S);
    cout<<"\n";
    }

    catch (const out_of_range& oor)
    {
    cerr << "Out of Range exception handled" << oor.what() << '\n';
    cout<<"\n";
    cout <<"FINAL SCHEDULE" << endl;
    print_schedule(S);
    cout<<"\n";
    }
}
