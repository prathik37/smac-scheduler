#ifndef MANIP_H_INCLUDED
#define MANIP_H_INCLUDED
#include <iostream>
#include <vector>

using namespace std;

void print_schedule(vector< vector<int> > a);
void sort_schedule(vector< vector<int> > &S);

void print_schedule( vector< vector<int> > a )
{
    for( auto rows : a)
    {
        for( auto cols : rows ) cout << cols << ' ';
        cout << '\n' ;
    }
}

void sort_schedule(vector< vector<int> > &S)
{
sort(begin(S),end(S),[]( const vector<int>& a, const vector<int>& b ) { return (a[1]+a[2]) < (b[1]+b[2]) ; } ) ;
}

#endif // MANIP_H_INCLUDED
